# Week1 - 10-12 November 2021

Day | Lesson 
---|---
10 November | [Module II Introduction](./slides/Machine_learning_systems_for_data_science.pdf) 
10 November | [Jupyter Notebook and Markdown Language, Python Libraries](./slides/Tools.pdf) 
10 November | [Numpy Library - Summary](./slides/library_numpy.pdf) 
11 November | [Numpy Library - Summary](./slides/library_numpy.pdf)
12 November | [Solution Exercise 1](./exercises/numpy_exercise.ipynb)
12 November | [Seies in Pandas Library](./slides/series_pandas.pdf) 
12 November | [Handling time with Pandas Library](./slides/handling_time_pandas.pdf)
12 November | [DataFrame in Pandas Library](./slides/dataframe_pandas.pdf)
17 November | [Solution Exercise 2](./exercises/Lesson3_Exercise.ipynb)





